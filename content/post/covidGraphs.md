+++
title = "Covid Data"
date = 2020-06-02T18:46:56-06:00
tags = ['covid']
+++

![covid_statue](/images/covid_statue.jpg)

There have been many opinions and critiques on how states have handled the covid pandemic. Florida has been was slow to enact restrictions and quick to loosen them. My state New Mexico was quick to enact restrictions and slow to release, if fact we are still required to wear masks in public. How do we know if these actions were the right actions to take.

There are many variables to consider but one way to determine if a state is being successful is to compare the deaths and death rates from covid relation to the ranking of population. For example NM is ranked 37th in population and 34th in deaths, where as Florida is 3rd and 11th respectively. There are conditions that would case a state to have more of their share of covid deaths, for example population density. There are other conditions that aren't apparent, Louisiana is ranked 25th in population but 9th in deaths, but doesn't have the population density of NY and NJ.

This is a table that shows the current data and rankings per state for deaths and testings [CURRENT].
For historical data per state [STATE]


[CURRENT]: /covidgraphs/current.html
[STATE]: /covidgraphs/
