---
title: "WI Covid Graphs"
date: 2020-07-04
---
WI Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidWIdeaths >}}
{{< covidgraphs/covidWIpositive >}}
{{< covidgraphs/covidWIrank >}}
{{< covidgraphs/covidWIhospital >}}
