---
title: "LA Covid Graphs"
date: 2020-07-04
---
LA Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidLAdeaths >}}
{{< covidgraphs/covidLApositive >}}
{{< covidgraphs/covidLArank >}}
{{< covidgraphs/covidLAhospital >}}
