---
title: "WA Covid Graphs"
date: 2020-07-04
---
WA Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidWAdeaths >}}
{{< covidgraphs/covidWApositive >}}
{{< covidgraphs/covidWArank >}}
{{< covidgraphs/covidWAhospital >}}
