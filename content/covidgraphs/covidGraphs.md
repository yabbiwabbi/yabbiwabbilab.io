+++
title = "CovidGraphs"
date = 2020-05-30T18:46:56-06:00
draft = true
tags = []
categories = []
+++

There have been many opinions on how states have handled the covid pandemic. People were upset that Florida was slow to enact restrictions and quick to loosen them. My state New Mexico was quick to enact restrictions and slow to release, if fact we are still required to wear masks in public.

One way to determine if a state is being successful is to compare the rate of deaths with other states in relation to the ranking of population. For example NM is ranked 37th in population and 21st in deaths per 100k, where as Florida is 3rd and 28th respectively. Granted there are other considerations that havent been taken into account, i.e. population density. 

This is a table that shows the current ranking per state for deaths and testings [RANKS].

The following are historical graphs for each state begining in March to present day.

[AK] [AL] [AR] [AZ] [CA] [CO] [CT] [CT] [DE] [FL] [GA] [HI] [IA] [ID] [IL] [IN] [KS] [KY] [LA] [MA] [MD] [ME] [MI] [MN] [MO] [MS] [MT] [NC] [ND] [NE] [NH] [NJ] [NM] [NV] [NY] [OH] [OK] [OR] [PA] [PR] [RI] [SC] [SD] [TN] [TX] [UT] [VA] [VT] [WA] [WI] [WV] [WY]


[RANKS]: /covidgraphs/rank_table.html
[AK]: /covidgraphs/covid_in_AK.html
[AL]: /covidgraphs/covid_in_AL.html
[AR]: /covidgraphs/covid_in_AR.html
[AZ]: /covidgraphs/covid_in_AZ.html
[CA]: /covidgraphs/covid_in_CA.html
[CO]: /covidgraphs/covid_in_CO.html
[CT]: /covidgraphs/covid_in_CT.html
[CT]: /covidgraphs/covid_in_DC.html
[DE]: /covidgraphs/covid_in_DE.html
[FL]: /covidgraphs/covid_in_FL.html
[GA]: /covidgraphs/covid_in_GA.html
[HI]: /covidgraphs/covid_in_HI.html
[IA]: /covidgraphs/covid_in_IA.html
[ID]: /covidgraphs/covid_in_ID.html
[IL]: /covidgraphs/covid_in_IL.html
[IN]: /covidgraphs/covid_in_IN.html
[KS]: /covidgraphs/covid_in_KS.html
[KY]: /covidgraphs/covid_in_KY.html
[LA]: /covidgraphs/covid_in_LA.html
[MA]: /covidgraphs/covid_in_MA.html
[MD]: /covidgraphs/covid_in_MD.html
[ME]: /covidgraphs/covid_in_ME.html
[MI]: /covidgraphs/covid_in_MI.html
[MN]: /covidgraphs/covid_in_MN.html
[MO]: /covidgraphs/covid_in_MO.html
[MS]: /covidgraphs/covid_in_MS.html
[MT]: /covidgraphs/covid_in_MT.html
[NC]: /covidgraphs/covid_in_NC.html
[ND]: /covidgraphs/covid_in_ND.html
[NE]: /covidgraphs/covid_in_NE.html
[NH]: /covidgraphs/covid_in_NH.html
[NJ]: /covidgraphs/covid_in_NJ.html
[NM]: /covidgraphs/covid_in_NM.html
[NV]: /covidgraphs/covid_in_NV.html
[NY]: /covidgraphs/covid_in_NY.html
[OH]: /covidgraphs/covid_in_OH.html
[OK]: /covidgraphs/covid_in_OK.html
[OR]: /covidgraphs/covid_in_OR.html
[PA]: /covidgraphs/covid_in_PA.html
[PR]: /covidgraphs/covid_in_PR.html
[RI]: /covidgraphs/covid_in_RI.html
[SC]: /covidgraphs/covid_in_SC.html
[SD]: /covidgraphs/covid_in_SD.html
[TN]: /covidgraphs/covid_in_TN.html
[TX]: /covidgraphs/covid_in_TX.html
[UT]: /covidgraphs/covid_in_UT.html
[VA]: /covidgraphs/covid_in_VA.html
[VT]: /covidgraphs/covid_in_VT.html
[WA]: /covidgraphs/covid_in_WA.html
[WI]: /covidgraphs/covid_in_WI.html
[WV]: /covidgraphs/covid_in_WV.html
[WY]: /covidgraphs/covid_in_WY.html
