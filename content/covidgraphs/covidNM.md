---
title: "NM Covid Graphs"
date: 2020-07-04
---
NM Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidNMdeaths >}}
{{< covidgraphs/covidNMpositive >}}
{{< covidgraphs/covidNMrank >}}
{{< covidgraphs/covidNMhospital >}}
