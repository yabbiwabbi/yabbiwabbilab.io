---
title: "VA Covid Graphs"
date: 2020-07-04
---
VA Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidVAdeaths >}}
{{< covidgraphs/covidVApositive >}}
{{< covidgraphs/covidVArank >}}
{{< covidgraphs/covidVAhospital >}}
