---
title: "Current stats per state"
date: 2020-07-04 15:43:14.299581
---
Covid Graphs from 20200403 to 20200703
This is a table that shows the current data and rankings per state for deaths and testings [CURRENT].

[CURRENT]: /covidgraphs/current.html
