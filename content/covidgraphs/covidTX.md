---
title: "TX Covid Graphs"
date: 2020-07-04
---
TX Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidTXdeaths >}}
{{< covidgraphs/covidTXpositive >}}
{{< covidgraphs/covidTXrank >}}
{{< covidgraphs/covidTXhospital >}}
