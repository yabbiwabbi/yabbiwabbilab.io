---
title: "VT Covid Graphs"
date: 2020-07-04
---
VT Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidVTdeaths >}}
{{< covidgraphs/covidVTpositive >}}
{{< covidgraphs/covidVTrank >}}
{{< covidgraphs/covidVThospital >}}
