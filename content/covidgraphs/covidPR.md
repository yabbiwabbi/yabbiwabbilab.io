---
title: "PR Covid Graphs"
date: 2020-07-04
---
PR Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidPRdeaths >}}
{{< covidgraphs/covidPRpositive >}}
{{< covidgraphs/covidPRrank >}}
{{< covidgraphs/covidPRhospital >}}
