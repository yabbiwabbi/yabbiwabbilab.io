---
title: "WV Covid Graphs"
date: 2020-07-04
---
WV Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidWVdeaths >}}
{{< covidgraphs/covidWVpositive >}}
{{< covidgraphs/covidWVrank >}}
{{< covidgraphs/covidWVhospital >}}
