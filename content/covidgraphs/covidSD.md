---
title: "SD Covid Graphs"
date: 2020-07-04
---
SD Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidSDdeaths >}}
{{< covidgraphs/covidSDpositive >}}
{{< covidgraphs/covidSDrank >}}
{{< covidgraphs/covidSDhospital >}}
