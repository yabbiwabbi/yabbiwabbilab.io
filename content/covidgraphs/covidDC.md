---
title: "DC Covid Graphs"
date: 2020-07-04
---
DC Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidDCdeaths >}}
{{< covidgraphs/covidDCpositive >}}
{{< covidgraphs/covidDCrank >}}
{{< covidgraphs/covidDChospital >}}
