---
title: "UT Covid Graphs"
date: 2020-07-04
---
UT Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidUTdeaths >}}
{{< covidgraphs/covidUTpositive >}}
{{< covidgraphs/covidUTrank >}}
{{< covidgraphs/covidUThospital >}}
