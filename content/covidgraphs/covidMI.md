---
title: "MI Covid Graphs"
date: 2020-07-04
---
MI Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidMIdeaths >}}
{{< covidgraphs/covidMIpositive >}}
{{< covidgraphs/covidMIrank >}}
{{< covidgraphs/covidMIhospital >}}
