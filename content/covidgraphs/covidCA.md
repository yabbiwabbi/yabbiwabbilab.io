---
title: "CA Covid Graphs"
date: 2020-07-04
---
CA Covid Graphs from 20200403 to 20200703
<!--more-->
{{< bokeh >}}
{{< covidgraphs/covidCAdeaths >}}
{{< covidgraphs/covidCApositive >}}
{{< covidgraphs/covidCArank >}}
{{< covidgraphs/covidCAhospital >}}
