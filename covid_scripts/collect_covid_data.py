import sqlite3
import requests
import os
import datetime
import threading
from collections import namedtuple
from bokeh.plotting import figure, output_file, show
from bokeh.layouts import gridplot
from bokeh.models import LinearAxis, Range1d

us_state_abbrev = {
    'Alabama': 'AL',
    'Alaska': 'AK',
    'American Samoa': 'AS',
    'Arizona': 'AZ',
    'Arkansas': 'AR',
    'California': 'CA',
    'Colorado': 'CO',
    'Connecticut': 'CT',
    'Delaware': 'DE',
    'District of Columbia': 'DC',
    'Florida': 'FL',
    'Georgia': 'GA',
    'Guam': 'GU',
    'Hawaii': 'HI',
    'Idaho': 'ID',
    'Illinois': 'IL',
    'Indiana': 'IN',
    'Iowa': 'IA',
    'Kansas': 'KS',
    'Kentucky': 'KY',
    'Louisiana': 'LA',
    'Maine': 'ME',
    'Maryland': 'MD',
    'Massachusetts': 'MA',
    'Michigan': 'MI',
    'Minnesota': 'MN',
    'Mississippi': 'MS',
    'Missouri': 'MO',
    'Montana': 'MT',
    'Nebraska': 'NE',
    'Nevada': 'NV',
    'New Hampshire': 'NH',
    'New Jersey': 'NJ',
    'New Mexico': 'NM',
    'New York': 'NY',
    'North Carolina': 'NC',
    'North Dakota': 'ND',
    'Northern Mariana Islands':'MP',
    'Ohio': 'OH',
    'Oklahoma': 'OK',
    'Oregon': 'OR',
    'Pennsylvania': 'PA',
    'Puerto Rico': 'PR',
    'Rhode Island': 'RI',
    'South Carolina': 'SC',
    'South Dakota': 'SD',
    'Tennessee': 'TN',
    'Texas': 'TX',
    'Utah': 'UT',
    'Vermont': 'VT',
    'Virgin Islands': 'VI',
    'Virginia': 'VA',
    'Washington': 'WA',
    'West Virginia': 'WV',
    'Wisconsin': 'WI',
    'Wyoming': 'WY'
}

# Data types for covidData table
data_type = [
             ['state', 'int'],
             ['date', 'text'],
             ['population', 'int'],
             ['dataQualityGrade', 'text'],
             ['death', 'int'],
             ['deathPer100k', 'float'],
             ['hospitalizedCumulative', 'int'],
             ['hospitalizedCurrently', 'int'],
             ['inIcuCumulative', 'int'],
             ['inIcuCurrently', 'int'],
             ['lastUpdateEt', 'text'],
             ['negative', 'int'],
             ['onVentilatorCumulative', 'int'],
             ['onVentilatorCurrently', 'int'],
             ['pending', 'int'],
             ['positive', 'int'],
             ['recovered', 'int']
            ]

def collect_data(state,population):
    # Each Thread needs to connect to the database
    conn = sqlite3.connect('covid.db')
    c = conn.cursor()

    placeholder = ", ".join(["?"] * len(insert_table_columns))
    start_day = datetime.datetime.strptime("20200403", "%Y%m%d").date()
    end_day = datetime.date.today()
    dates_generated = [start_day + datetime.timedelta(days=x) for x in range(0, (end_day-start_day).days)]
    data = []
    for d in dates_generated:
        date = d.strftime("%Y%m%d")
        #print("Collecting data for state {} and date {}. Column Length = {}".format(state.lower(),date,len(insert_table_columns)))
        stmt = "https://covidtracking.com/api/v1/states/{}/{}.json".format(state.lower(), date)
        response = requests.get(stmt)
        state_covid = response.json()
    
        stmt = "REPLACE INTO `{table}` ({columns}) VALUES ({values});".format(table='covidData', columns=",".join(insert_table_columns), values=placeholder)
        data.append([state_covid['state'],
                    state_covid['date'],
                    population,
                    state_covid['dataQualityGrade'],
                    state_covid['death'],
                    (state_covid['death']/ population) * 100000,
                    state_covid['hospitalizedCumulative'],
                    state_covid['hospitalizedCurrently'],
                    state_covid['inIcuCumulative'],
                    state_covid['inIcuCurrently'],
                    state_covid['lastUpdateEt'],
                    state_covid['negative'],
                    state_covid['onVentilatorCumulative'],
                    state_covid['onVentilatorCurrently'],
                    state_covid['pending'],
                    state_covid['positive'],
                    state_covid['recovered']])

    with lock:
        #print("saving data for {state} for period {start} to  {end}".format(state=state,start=start_day.strftime("%Y%m%d"),end=end_day.strftime("%Y%m%d")))
        c.executemany(stmt, data)
        conn.commit()

def make_ranks():
    #print("Sorting and ranking the data per state per day")
    # Create rank table if it doesnt exist
    c.execute("CREATE TABLE IF NOT EXISTS ranks (state text, date text, popRank int, deathRank int, deathPer100kRank int, PRIMARY KEY (state,date))")

    start_day = datetime.datetime.strptime("20200403", "%Y%m%d").date()
    end_day = datetime.date.today()
    dates_generated = [start_day + datetime.timedelta(days=x) for x in range(0, (end_day-start_day).days)]
    ranks = {} 
    for d in dates_generated:
        date = d.strftime("%Y%m%d")
        # Get Sorted list of items we want to rank
        c.execute('SELECT state, population FROM population ORDER BY population DESC')
        pop_list = c.fetchall()
        c.execute('SELECT state, death FROM covidData WHERE date = ? ORDER BY death DESC',(date,))
        death_list = c.fetchall()
        c.execute('SELECT state, deathPer100k FROM covidData WHERE date = ? ORDER BY deathPer100k DESC',(date,))
        per100k_list = c.fetchall()
        #c.execute('SELECT state, totalTestResults FROM covidData WHERE date = ? ORDER BY totalTestResults DESC',(date,))
        #tests_list = c.fetchall()
        #c.execute('SELECT state, testPercent FROM covidData WHERE date = ? ORDER BY testPercent DESC',(date,))
        #test_percent = c.fetchall()
        
        rankKey = namedtuple("rankKey", ["state", "date"])
        
        for count, item in enumerate(pop_list):
            state = item[0]
            r = rankKey(state=state, date=date)
            ranks[r] = {'state': state, 'date': date}
            ranks[r]['popRank'] = count + 1
        
        for count, item in enumerate(death_list):
            state = item[0]
            r = rankKey(state=state, date=date)
            ranks[r]['deathRank'] = count + 1
    
        for count, item in enumerate(per100k_list):
            state = item[0]
            r = rankKey(state=state, date=date)
            ranks[r]['per100kRank'] = count + 1
        
        #for count, item in enumerate(tests_list):
        #    state = item[0]
        #    r = rankKey(state=state, date=date)
        #    ranks[r]['testsRank'] = count + 1
        
        #for count, item in enumerate(test_percent):
        #    state = item[0]
        #    r = rankKey(state=state, date=date)
        #    ranks[r]['testPercentRank'] = count + 1

    rank_list = []
    for rank in ranks:
        entry = [
                 ranks[rank]['state'],
                 ranks[rank]['date'],
                 ranks[rank]['popRank'],
                 ranks[rank]['deathRank'],
                 ranks[rank]['per100kRank'],
                 #ranks[rank]['testsRank'],
                 #ranks[rank]['testPercentRank']
                ]
        rank_list.append(entry)

    c.executemany('REPLACE INTO ranks VALUES (?,?,?,?,?)', rank_list)
    conn.commit()


# Connect to sqlite database
conn = sqlite3.connect('covid.db')
c = conn.cursor()

# Create Population table if it doesn't exist
c.execute("CREATE TABLE IF NOT EXISTS population (state text, fullName text, population int, popRank int, PRIMARY KEY (state))")

# Check to see if there is data in the population table
c.execute('SELECT * FROM population ORDER BY population DESC')
pop_list = c.fetchall()
if len(pop_list) == 0:
    # table is empty
    # Collect population data and enter in table. A key from api.census.gov is required
    #print("Collecting population data")
    response = requests.get("https://api.census.gov/data/2019/pep/population?get=POP,NAME&for=state:*&key={}".format(os.environ['CENSUS_KEY']))
    states_pop = response.json()

    state_list = []
    for state_pop in states_pop[1:]:
        pop = int(state_pop[0])
        state = us_state_abbrev[state_pop[1]]
        fullName = state_pop[1]
        state_list.append([state, fullName, pop])

    c.executemany('INSERT INTO population (state, fullName, population) VALUES (?,?,?)', state_list)
    conn.commit()

insert_table_columns = []
create_table_columns =[]
for entry in data_type:
    insert_table_columns.append(entry[0])
    create_table_columns.append(" ".join(entry))

stmt = "CREATE TABLE IF NOT EXISTS covidData ({columns}, PRIMARY KEY (state, date))".format(columns=",".join(create_table_columns))
c.execute(stmt)

c.execute('SELECT * FROM population ORDER BY population DESC')
pop_list = c.fetchall()

threads = {}
lock = threading.Lock()
for pop in pop_list:
    state = pop[0]
    # Get Population for the state
    c.execute('SELECT population FROM population where state = ?',(state,))
    rows = c.fetchall()
    population = rows[0][0]
    threads[state] = threading.Thread( target=collect_data, args=(state, population))
    #print("starting thread for {}".format(state))
    threads[state].start()

# Join Threads
for state in threads:
    #print("joining thread for {}".format(state))
    threads[state].join()

make_ranks()
