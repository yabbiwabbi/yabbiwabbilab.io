import requests
import sqlite3
import glob
import csv
from datetime import datetime, timedelta, date
from random import randint
from bokeh.plotting import figure, output_file, show, save
from bokeh.layouts import gridplot
from bokeh.embed import components
from bokeh.models import LinearAxis, Range1d
from bokeh.models import ColumnDataSource, DataTable, DateFormatter, TableColumn


def graph_ranks(state):
    conn = sqlite3.connect('covid.db')
    c = conn.cursor()

    #stmt = 'SELECT state,date,popRank,deathRank,deathPer100kRank,testsRank,testsPercentRank FROM ranks WHERE state = ? ORDER BY date'
    stmt = 'SELECT state,date,popRank,deathRank,deathPer100kRank FROM ranks WHERE state = ? ORDER BY date'
    c.execute(stmt, (state,))
    data_list = c.fetchall()
    days=list(range(1,len(data_list)+1))
    popRank = []
    deathRank = []
    per100kRank = []
    #testsRank = []
    #testPercentRank = []
    for data in data_list:
        popRank.append(data[2])
        deathRank.append(data[3])
        per100kRank.append(data[4])
        #testsRank.append(data[5])
        #testPercentRank.append(data[6])
    plot = figure(title="Ranking for {}".format(state),
                  x_axis_label = 'Days',
                  y_axis_label = 'Ranking',
                  y_range = (55, 1))
    plot.line(days, popRank, legend_label='Population Rank', line_width=2, color = 'red')
    plot.line(days, deathRank, legend_label='Death Rank', line_width=2, color = 'blue')
    plot.line(days, per100kRank, legend_label='Deaths Per 100k Rank', line_width=2, color = 'green')
    #plot.line(days, testsRank, legend_label='Tests Rank', line_width=2, color = 'yellow')
    #plot.line(days, testPercentRank, legend_label='Test% Rank', line_width=2, color = 'orange')
    plot.legend.location = "bottom_left"
    return(plot)

def graph_positive(state):
    conn = sqlite3.connect('covid.db')
    c = conn.cursor()

    # Set Min and Max for y1 and y2 col
    y1_stmt = 'SELECT positive FROM covidData WHERE state = ? ORDER BY positive'
    c.execute(y1_stmt, (state,))
    y1_list = c.fetchall()
    y1_min = y1_list[0][0]
    y1_max = y1_list[len(y1_list)-1][0]

    # get a collolated list for graphings
    stmt = 'SELECT state, date, positive FROM covidData WHERE state = ? ORDER BY date'
    c.execute(stmt, (state,))
    data_list = c.fetchall()
    y1_data = []
    for data in data_list:
        y1_data.append(data[2])

    days=list(range(1,len(data_list)+1))
    plot = figure(title="Tested Positive in {}".format(state),
                  x_axis_label = 'Days',
                  y_axis_label = 'Positive Tests',
                  y_range = (0, y1_max))
    # Right y axis
    plot.line(days, y1_data, legend_label='Positive Tests', line_width=2)
    plot.legend.location = "top_left"

    return(plot)

def graph_hospital(state):
    conn = sqlite3.connect('covid.db')
    c = conn.cursor()

    # Set Min and Max for y1 and y2 col
    y1_stmt = 'SELECT hospitalizedCurrently FROM covidData WHERE state = ? ORDER BY hospitalizedCurrently'
    c.execute(y1_stmt, (state,))
    y1_list = c.fetchall()
    y1_min = y1_list[0][0]
    y1_max = y1_list[len(y1_list)-1][0]

    # get a collolated list for graphings
    stmt = 'SELECT state, date, hospitalizedCurrently FROM covidData WHERE state = ? ORDER BY date'
    c.execute(stmt, (state,))
    data_list = c.fetchall()
    y1_data = []
    for data in data_list:
        y1_data.append(data[2])

    days=list(range(1,len(data_list)+1))
    plot = figure(title="Currently in Hospital in {}".format(state),
                  x_axis_label = 'Days',
                  y_axis_label = 'In Hospital',
                  y_range = (0, y1_max))
    # Right y axis
    plot.line(days, y1_data, legend_label='In Hospital', line_width=2)
    plot.legend.location = "top_left"

    return(plot)


# Graph death and tests
def graph_states(state, g):
    conn = sqlite3.connect('covid.db')
    c = conn.cursor()

    # Set Min and Max for y1 and y2 col
    y1_stmt = 'SELECT {} FROM covidData WHERE state = ? ORDER BY {}'.format(g['y1_col'],g['y1_col'])
    c.execute(y1_stmt, (state,))
    y1_list = c.fetchall()
    y1_min = y1_list[0][0]
    y1_max = y1_list[len(y1_list)-1][0]
    y2_stmt = 'SELECT {} FROM covidData WHERE state = ? ORDER BY {}'.format(g['y2_col'],g['y2_col'])
    c.execute(y2_stmt, (state,))
    y2_list = c.fetchall()
    y2_min = y2_list[0][0]
    y2_max = y2_list[len(y2_list)-1][0]

    # get a collolated list for graphings
    stmt = 'SELECT state, date, {}, {} FROM covidData WHERE state = ? ORDER BY date'.format(g['y1_col'],g['y2_col'])
    c.execute(stmt, (state,))
    data_list = c.fetchall()
    y1_data = []
    y2_data = []
    for data in data_list:
        y1_data.append(data[2])
        y2_data.append(data[3])

    days=list(range(1,len(data_list)+1))
    plot = figure(title=g['title'].format(state),
                  x_axis_label = g['x_axis_label'],
                  y_axis_label = g['y1_axis_label'],
                  y_range = (0, y1_max))
    # Right y axis
    plot.line(days, y1_data, legend_label=g['y1_axis_label'], line_width=2)
    # Left y axis
    plot.extra_y_ranges = { 'y_col2_range': Range1d( y2_min, y2_max * 1.10) }
    plot.add_layout(LinearAxis(y_range_name = 'y_col2_range', axis_label = g['y2_axis_label']), "right")
    plot.line(days, y2_data, legend_label = g['y2_axis_label'], line_width=2, y_range_name='y_col2_range', color = 'red')

    plot.legend.location = "top_left"

    return(plot)

def graph_table():

    output_file( "{}/content/covidgraphs/current.html".format(base_dir))
    # get latest date
    stmt = 'SELECT date FROM ranks WHERE state = "NM" ORDER BY date DESC'
    c.execute(stmt, ())
    last_date = c.fetchall()[0][0]

    # Collect Rankings and store in individual lists
    #stmt = 'SELECT state,date,popRank,deathRank,deathPer100kRank,testsRank,testsPercentRank FROM ranks WHERE date = ? ORDER BY popRank'
    stmt = 'SELECT state,date,popRank,deathRank,deathPer100kRank FROM ranks WHERE date = ? ORDER BY popRank'
    c.execute(stmt, (last_date,))
    rank_list = c.fetchall()
    states = []
    popRank = []
    deathRank = []
    deathPer100kRank = []
    #testsRank = []
    #testPercentRank = []
    for rank in rank_list:
        states.append(rank[0])
        popRank.append(rank[2])
        deathRank.append(rank[3])
        deathPer100kRank.append(rank[4])
        #testsRank.append(rank[5])
        #testPercentRank.append(rank[6])

    # Collect covidData and store in individual lists
    #stmt = 'SELECT state,date,popRank,deathRank,deathPer100kRank,testsRank,testsPercentRank FROM ranks WHERE date = ? ORDER BY popRank'
    stmt = 'SELECT state,date,death,positive,negative FROM covidData WHERE date = ? ORDER BY state'
    c.execute(stmt, (last_date,))
    rank_list = c.fetchall()
    death = []
    deathPer100k = []
    positive = []
    negative = []
    for state in states:
        stmt = 'SELECT state,date,death,deathPer100k,positive,negative FROM covidData WHERE date = ? and state = ?'
        c.execute(stmt, (last_date,state))
        data_list = c.fetchall()
        death.append(data_list[0][2])
        deathPer100k.append(data_list[0][3])
        positive.append(data_list[0][4])
        negative.append(data_list[0][5])

    data = dict(
        states=states,
        popRank=popRank,
        death=death,
        deathRank=deathRank,
        deathPer100k=deathPer100k,
        deathPer100kRank=deathPer100kRank,
        positive=positive,
        negative=negative,
        #testsRank=testsRank,
        #testPercentRank=testPercentRank)
        )
    source = ColumnDataSource(data)

    columns = [ 
                TableColumn(field="states", title="State"),
                TableColumn(field="popRank", title="Population Rank"),
                TableColumn(field="death", title="Deaths"),
                TableColumn(field="deathRank", title="Death Rank"),
                TableColumn(field="deathPer100k", title="Death per 100K"),
                TableColumn(field="deathPer100kRank", title="Death per 100K Rank"),
                TableColumn(field="positive", title="Positive Tests"),
                TableColumn(field="negative", title="Negative Tests"),
                #TableColumn(field="testsRank", title="Tests Rank"),
                #TableColumn(field="testPercentRank", title="Test Percent Rank")]
              ]
    data_table = DataTable(source=source, columns=columns, width=800, height=480)

    save(data_table)
    return(data_table)

# Connect to sqlite database
conn = sqlite3.connect('covid.db')
c = conn.cursor()
c.execute('SELECT state FROM population ORDER BY state')
state_list = c.fetchall()

# Get first and last date
stmt = 'SELECT date FROM covidData WHERE state = "NM" ORDER BY date'
c.execute(stmt)
date_list = c.fetchall()
start = date_list[0][0]
end = date_list[len(date_list)-1][0]

# Base Directory
base_dir = "../"
# File names
html_fname = base_dir + "/layouts/shortcodes/covidgraphs/covid{}{}.html"
md_fname = base_dir + "/content/covidgraphs/covid{}.md"
table_fname = base_dir + "/layouts/shortcodes/covidgraphs/current.html"
table_md_fname = base_dir + "/content/covidgraphs/current_list.md"

# State table of current stats
table = graph_table()
file = open(table_fname,'w')
script, div = components(table) 
file.write(div)
file.write("<body>")
file.write(script)
file.write("</body>")
file.close()

file = open(table_md_fname,'w')
file.write('---\n')
file.write('title: "Current stats per state"\n')
file.write('date: {date}\n'.format(date = datetime.now() - timedelta(hours=0, minutes=10)))
file.write('---\n')
file.write('Covid Graphs from {start} to {end}\n'.format(start = start, end = end))
file.writelines(['This is a table that shows the current data and rankings per state for deaths and testings [CURRENT].\n',
                 '\n',
                 '[CURRENT]: /covidgraphs/current.html\n'])
file.close()

for state in sorted(state_list):
    plots = [] 
    #output_file("./graphs/covid_in_{}.html".format(state[0]))


    d = {'title' : "Covid deaths and death rate {}",
         'x_axis_label': 'Day',
         'y1_axis_label': 'Deaths per 100k',
         'y1_col': 'deathPer100k',
         'y2_axis_label': 'Deaths',
         'y2_col': 'death'}
    dplots = graph_states(state[0], d)
    file = open(html_fname.format(state[0],'deaths'),'w')
    script, div = components(dplots) 
    file.write(div)
    file.write("\n")
    file.write("\n<body>\n")
    file.write(script)
    file.write("\n")
    file.write("\n</body>\n")
    file.close()

    #t = {'title' : "Covid tests and test percentage {}",
         #'x_axis_label': 'Day',
         #'y1_axis_label': 'Test Percentage',
         #'y1_col': 'testPercent',
         #'y2_axis_label': 'total tests',
         #'y2_col': 'totalTestResults'}
    #tplots = graph_states(state[0], t)
    #file = open(html_fname.format(state[0],'tests'),'w')
    #script, div = components(tplots) 
    #file.write(div)
    #file.write("<body>")
    #file.write(script)
    #file.write("</body>")
    #file.close()

    hoplots = graph_hospital(state[0])
    file = open(html_fname.format(state[0],'hospital'),'w')
    script, div = components(hoplots) 
    file.write(div)
    file.write("<body>")
    file.write(script)
    file.write("</body>")
    file.close()

    poplots = graph_positive(state[0])
    file = open(html_fname.format(state[0],'positive'),'w')
    script, div = components(poplots) 
    file.write(div)
    file.write("<body>")
    file.write(script)
    file.write("</body>")
    file.close()

    rankplots = graph_ranks(state[0])
    file = open(html_fname.format(state[0],'rank'),'w')
    script, div = components(rankplots) 
    file.write(div)
    file.write("<body>")
    file.write(script)
    file.write("</body>")
    file.close()

    file = open(md_fname.format(state[0]),'w')
    file.write('---\n')
    file.write('title: "{state} Covid Graphs"\n'.format(state = state[0]))
    file.write('date: {date}\n'.format(date = date.today()))
    file.write('---\n')
    file.write('{state} Covid Graphs from {start} to {end}\n'.format(state = state[0],start = start, end = end))
    file.write('<!--more-->\n')
    file.write('{{< bokeh >}}\n')
    file.write('{{{{< covidgraphs/covid{state}deaths >}}}}\n'.format(state = state[0]))
    file.write('{{{{< covidgraphs/covid{state}positive >}}}}\n'.format(state = state[0]))
    file.write('{{{{< covidgraphs/covid{state}rank >}}}}\n'.format(state = state[0]))
    file.write('{{{{< covidgraphs/covid{state}hospital >}}}}\n'.format(state = state[0]))
    #file.write('{{{{< covidgraphs/covid{state}tests >}}}}\n'.format(state = state[0]))
